function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Produce units passed in a list. Have option to clear the buildlist at the beginning. Distribute the order over all factories in group. Fails if no factory in a group can build some unit from the list.",
		parameterDefs = {
			{ 
				name = "productionList",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{[1] = {unitName = 'armpw', count = 10}}",
			},
			--[[
			example:
			{
				[1] = {unitName = "armpw", count = 10},
				[2] = {unitName = "armrock", count = 3},
				[3] = {unitName = "armpw", count = 5},
 			}
			
			]]--
			{ 
				name = "clearAtStart",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "true",
			},
			{ 
				name = "successOnEmptyQueue",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "true",
			},
		}
	}
end

-- constants

-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetFactoryCommands = Spring.GetFactoryCommands

local function ClearState(self)
	self.productionStarted = false
end

function Run(self, units, parameter)
	local productionList = parameter.productionList -- table
	local clearAtStart = parameter.clearAtStart -- boolean
	local successOnEmptyQueue = parameter.successOnEmptyQueue -- boolean
	
	-- clear commands if needed
	if clearAtStart then
		-- TBD
	end
	
	if (not self.productionStarted) then
		-- go one by one through factories and try to distribute all orders to them
		-- FAIL in case of no factory can build any unit from the order
		for listItemIndex = 1, #productionList do
			local thisItem = productionList[listItemIndex]
			local thisItemUnitDefID = UnitDefNames[thisItem.unitName].id
			for c=1, thisItem.count do
				for i=1, #units do
					SpringGiveOrderToUnit(units[i], -thisItemUnitDefID, {}, {})
					break --temp, later conditional
				end
			end
		end	
		
		if (successOnEmptyQueue) then
			self.productionStarted = true
			return RUNNING
		else
			return SUCCESS
		end
	else
		local factoryCommandsCount = 0
		
		for i=1, #units do
			factoryCommandsCount = factoryCommandsCount + SpringGetFactoryCommands(units[i], 0)
		end
		
		if (factoryCommandsCount > 0) then
			return RUNNING
		else
			return SUCCESS
		end
	end	
end


function Reset(self)
	ClearState(self)
end
